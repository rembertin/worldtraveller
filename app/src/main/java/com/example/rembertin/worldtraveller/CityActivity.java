package com.example.rembertin.worldtraveller;

import android.app.ActionBar;
import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;

public class CityActivity extends AppCompatActivity {
    private TextView villeTextView;
    private TextView departementValueTextView;
    private TextView regionValueTextView;
    private TextView superficieValueTextView;
    private TextView populationValueTextView;
    private TextView gpsValueTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        Intent intent = getIntent();
        String cityName = intent.getStringExtra("cityName");
        setTitle(getString(R.string.titlePartial) + cityName);
        initAttributes();
        new GetDataTask(this).execute(cityName);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("ville", villeTextView.getText().toString());
        outState.putString("departement", departementValueTextView.getText().toString());
        outState.putString("region", regionValueTextView.getText().toString());
        outState.putString("superficie", superficieValueTextView.getText().toString());
        outState.putString("population", populationValueTextView.getText().toString());
        outState.putString("gps", gpsValueTextView.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        villeTextView.setText(savedInstanceState.getString("cityName"));
        departementValueTextView.setText(savedInstanceState.getString("departement"));
        regionValueTextView.setText(savedInstanceState.getString("region"));
        superficieValueTextView.setText(savedInstanceState.getString("superficie"));
        populationValueTextView.setText(savedInstanceState.getString("population"));
        gpsValueTextView.setText(savedInstanceState.getString("gps"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initAttributes() {
        this.villeTextView = (TextView) findViewById(R.id.ville);
        this.departementValueTextView = (TextView) findViewById(R.id.departementValue);
        this.regionValueTextView = (TextView) findViewById(R.id.regionValue);
        this.superficieValueTextView = (TextView) findViewById(R.id.superficieValue);
        this.populationValueTextView = (TextView) findViewById(R.id.populationValue);
        this.gpsValueTextView = (TextView) findViewById(R.id.gpsValue);
    }

    public void updateCityInfos(String responseString) {
        try {
            JSONObject jsonObject = new JSONObject(responseString);
            JSONObject jsonFields = jsonObject.getJSONArray("records").getJSONObject(0).getJSONObject("fields");
            villeTextView.setText(jsonFields.getString("nom_comm"));
            departementValueTextView.setText(jsonFields.getString("nom_dept"));
            regionValueTextView.setText(jsonFields.getString("nom_region"));
            superficieValueTextView.setText(jsonFields.getString("superficie") + getString(R.string.superficieUnit));
            populationValueTextView.setText(Float.parseFloat(jsonFields.getString("population"))*1000 + getString(R.string.populationUnit));
            JSONArray jsonGPSCoords = jsonFields.getJSONArray("geo_point_2d");
            String latitude = new DecimalFormat("#.##").format(jsonGPSCoords.getDouble(0));
            String longitude = new DecimalFormat("#.##").format(jsonGPSCoords.getDouble(1));
            gpsValueTextView.setText(latitude + " ; " + longitude);
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(this, R.string.toastError, Toast.LENGTH_LONG).show();
            NavUtils.navigateUpFromSameTask(this);
        }
    }
}