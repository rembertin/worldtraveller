package com.example.rembertin.worldtraveller;

import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;

import com.example.rembertin.worldtraveller.database.City;
import com.example.rembertin.worldtraveller.database.CitySqlOpenHelper;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private Button searchButton;
    private AutoCompleteTextView cityInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        searchButton = (Button) findViewById(R.id.searchButton);
        cityInput = (AutoCompleteTextView) findViewById(R.id.cityInput);
        cityInput.setText(getPreferences(MODE_PRIVATE).getString("lastCitySearched", ""));
        initGpsCoordinates();
        CitySqlOpenHelper db = new CitySqlOpenHelper(this);
        List<City> cities = db.getAllCities();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.autocomplete_list_item);
        for (City city : cities) {
            adapter.add(city.getName());
        }
        cityInput.setAdapter(adapter);
    }

    private void initGpsCoordinates() {
        GPSTracker gpsTracker = new GPSTracker(this);
        double latitude = gpsTracker.getLatitude();
        double longitude = gpsTracker.getLongitude();
        TextView gpsMainValueTextView = (TextView) findViewById(R.id.gpsMainValue);
        gpsMainValueTextView.setText(new DecimalFormat("#.##").format(latitude) + " ; "
                + new DecimalFormat("#.##").format(longitude));

        Geocoder gcd = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = gcd.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0)
                System.out.println(addresses.get(0).getLocality());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void searchOnClick(View view) {
        String citySearched = cityInput.getText().toString();
        if (view == searchButton) {
            SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();
            editor.putString("lastCitySearched", citySearched);
            editor.apply();
            CitySqlOpenHelper db = new CitySqlOpenHelper(this);
            db.addCityIfNotPresent(new City(citySearched));
            Intent cityAtivityIntent = new Intent(this, CityActivity.class);
            cityAtivityIntent.putExtra("cityName", citySearched);
            startActivity(cityAtivityIntent);
        }
    }
}