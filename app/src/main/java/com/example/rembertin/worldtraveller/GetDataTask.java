package com.example.rembertin.worldtraveller;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by rembertin on 19/11/15.
 */
public class GetDataTask extends AsyncTask<String, Void, String> {
    private final CityActivity callingActivity;
    private ProgressDialog progressDialog;

    public GetDataTask(CityActivity callingActivity) {
        this.callingActivity = callingActivity;
    }

    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(callingActivity);
        progressDialog.setMessage("Veuillez patienter...");
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... strings) {
        URL url;
        String responseString = null;
        HttpURLConnection connection = null;
        try {
            final String urlString = "http://public.opendatasoft.com/api/records/1.0/search?dataset=correspondance-code-insee-code-postal&q=" + URLEncoder.encode(strings[0], "utf-8");
            url = new URL(urlString);
            connection = (HttpURLConnection)url.openConnection();
            InputStream in = new BufferedInputStream(connection.getInputStream());
            responseString = readStream(in);
            Log.i("HTTP", responseString);
        } catch (MalformedURLException e) {
            cancel(true);
            return null;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return responseString;
    }

    private String readStream(InputStream in) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder result = new StringBuilder();
        String line;
        try {
            while((line = reader.readLine()) != null) {
                result.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result.toString();
    }

    @Override
    protected void onPostExecute(String s) {
        callingActivity.updateCityInfos(s);
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
