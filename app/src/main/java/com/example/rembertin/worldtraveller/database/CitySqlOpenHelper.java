package com.example.rembertin.worldtraveller.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by rembertin on 09/12/15.
 */
public class CitySqlOpenHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "CityDB";
    public static final String CITIES_TABLE = "cities";
    public static final String ID_COLUMN = "id";
    public static final String NAME_COLUMN = "name";

    private static final String[] COLUMNS = {ID_COLUMN,NAME_COLUMN};

    public CitySqlOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // SQL statement to create cities table
        String CREATE_CITIES_TABLE = "CREATE TABLE " + CITIES_TABLE + " ( " +
                ID_COLUMN + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                NAME_COLUMN + " TEXT )";

        // create cities table
        db.execSQL(CREATE_CITIES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older cities table if existed
        db.execSQL("DROP TABLE IF EXISTS " + CITIES_TABLE);

        // create fresh cities table
        this.onCreate(db);
    }

    public void addCityIfNotPresent(City city) {
        if (! isPresent(city.getName())) {
            // 1. get reference to writable DB
            SQLiteDatabase db = this.getWritableDatabase();

            // 2. create ContentValues to add key "column"/value
            ContentValues values = new ContentValues();
            values.put(NAME_COLUMN, city.getName()); // get title

            // 3. insert
            db.insert(CITIES_TABLE, // table
                    null, //nullColumnHack
                    values); // key/value -> keys = column names/ values = column values

            // 4. close
            db.close();
        }
    }

    private boolean isPresent(String name) {
        String sqlQuery = "SELECT  * FROM " + CITIES_TABLE;
        // 2. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sqlQuery, null);
        boolean isPresent = cursor.moveToFirst();
        cursor.close();
        db.close();
        return isPresent;
    }

    public List<City> getAllCities() {
        List<City> cities = new LinkedList<City>();

        // 1. build the query
        String query = "SELECT  * FROM " + CITIES_TABLE;

        // 2. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        // 3. go over each row, build city and add it to list
        City city = null;
        if (cursor.moveToFirst()) {
            do {
                city = new City();
                city.setId(Integer.parseInt(cursor.getString(0)));
                city.setName(cursor.getString(1));

                // Add city to cities
                cities.add(city);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        // return cities
        return cities;
    }
}